var reset = $('#new');
var updateTirage = $('#update');
var random = $('#random');

reset.hide();
updateTirage.hide();

random.click(function () {
    $('#form-participants').hide();
    $('#list-participants').hide();

    $('#random, #new, #update').hide();

    $('#winner span').remove();

    var items = [];
    $(function () {
        $('#list-participants li').each(function () {
            items.push($(this).text());
        });
        var item = items[Math.floor(Math.random() * items.length)];

        // show the results
        var myCounter = new Countdown({
            seconds: 3,  // number of seconds to count down
            onUpdateStatus: function (sec) {
                $('#winner').empty();
                if (sec === 0) {
                    return false
                } else {
                    $('#winner').append(`<span class="countdown">${sec}</span>`);
                }
            }, // callback for each second
            onCounterEnd: function () {
                $('#winner').append(`<span>Bravo ${item} !</span>`);

                $('#random, #new, #update').hide(0).delay(5000).show(500);
            } // final action
        });

        myCounter.start();
    });

});

$(document).ready(function ($) {
    $('form').submit(function (e) {
        e.preventDefault();
        if ($('.input').val() !== '') {
            var newTask = $('.input').val();
            var newLi = $(`<li>${newTask} <i class="fa fa-times"></i></li>`);
            $('.fa-times', newLi).on('click', function () {
                $(this).parent('li').remove(); // Attach the event handler *before* adding the element
            });
            $('ul').prepend(newLi); // To put the new task at the top of the list
            $('.input').val('');

            // console.log($('#list-participants li').length);
            //
            // if ($('#list-participants li') === 1) {
            //   $('#random').prop("disabled", false);
            // }
            return false; // So the change persists
        } else {
            return false;
        }
    });
    // $('ul').sortable(); // Because what good is a to-do list that you can't sort? :)
});

function update() {
    $('#form-participants').show();
    $('#list-participants').show();
    $('#new, #update, #winner span').hide();
}

function Countdown(options) {
    var timer,
        instance = this,
        seconds = options.seconds || 10,
        updateStatus = options.onUpdateStatus || function () {
        },
        counterEnd = options.onCounterEnd || function () {
        };

    function decrementCounter() {
        updateStatus(seconds);
        if (seconds === 0) {
            counterEnd();
            instance.stop();
        }
        seconds--;
    }

    this.start = function () {
        clearInterval(timer);
        timer = 0;
        seconds = options.seconds;
        timer = setInterval(decrementCounter, 1000);
    };

    this.stop = function () {
        clearInterval(timer);
    };
}